<?php

namespace App\Repository;

use App\Entity\Article;
use App\Utils\ConnectUtils;

class ArticleRepository
{
  private $connection;

  public function __construct()
  {
    try {

      $this->connection = ConnectUtils::getConnection();

    } catch (\PDOException $e) {

      dump($e);

    }
  }

  public function update(Article $article)
  {

    try {
      $cnx = ConnectUtils::getConnection();

      $query = $cnx->prepare("UPDATE article SET title = :title, category = :category, image = :image, content = :content WHERE id = :id");

      $query->bindValue(":title", $article->title);
      $query->bindValue(":category", $article->category);
      $query->bindValue(":image", $article->image);
      $query->bindValue(":content", $article->content);
      $query->bindValue(":id", $article->id);

      return $query->execute();

    } catch (\PDOException $e) {
      dump($e);
    }
  }
  public function delete(int $id)
  {
    try {

      $cnx = ConnectUtils::getConnection();

      $query = $cnx->prepare("DELETE FROM article WHERE id = :id");

      $query->bindValue(":id", $id);

      return $query->execute();

    } catch (\PDOException $e) {
      dump($e);
    }

  }

  private function fetch(string $query, array $params = [])
  {
    try {

      $cnx = ConnectUtils::getConnection();
      $query = $cnx->prepare($query);

      foreach ($params as $param => $value) {
        $query->bindValue($param, $value);
      }

      $query->execute();

      $result = [];
      foreach ($query->fetchAll() as $row) {
        $result[] = Article::fromSQL($row);
      }

      if (count($result) <= 1) {
        return $result[0];
      }

      return $result;

    } catch (\PDOException $e) {
      dump($e);
    }

  }

  public function add(Article $article)
  {
    $this->fetch(
      "INSERT INTO article (title, category, image, content) VALUES (:title, :category, :image, :content)",
      [
        ":title" => $article->title,
        ":category" => $article->category,
        ":image" => $article->image,
        ":content" => $article->content
      ]
    );

    $article->id = intval($this->connection->lastInsertId());

    return $article;
  }




  public function get(int $id)
  {
    return $this->fetch("SELECT * FROM article WHERE id=:id", [":id" => $id]);
  }




  public function getAll()
  {
    return $this->fetch("SELECT * FROM article");
  }


}