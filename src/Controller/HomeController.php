<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ArticleRepository;


class HomeController extends AbstractController
{

  /**
   * @Route("/", name="homepage")
   */

  public function index(ArticleRepository $repo)
  {
    
    $result = $repo->getAll();
    if(!is_array($result)){
      $result = [$result];
    }
    return $this->render("homepage.html.twig", [
      'controller_name' => 'HomeController',
      'result' => $result
    ]);
  }

}
