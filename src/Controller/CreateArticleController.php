<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class CreateArticleController extends Controller
{
    /**
     * @Route("/create_article", name="create_article")
     */

    public function index(ArticleRepository $repo, Request $request)
    {
        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // $article->image->move(dirname(FILE) . "/../../public/upload", "exemple.jpg");
            // $article->image = "upload/exemple.jpg";
            $repo->add($form->getData());
            return $this->redirectToRoute("homepage");
        }

        return $this->render('create_article.html.twig', [
            'form' => $form->createView()
        ]);
    }
}