<?php

namespace App\Controller;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\UserRepository;
use App\Form\UserType;
use App\Entity\User;


class UserController extends Controller
{
    /**
     * @Route("/user", name="user")
     * @Security("has_role('ROLE_USER')")
     */
    public function index(Request $request, UserRepository $userRepository)
    {
        return $this->render('user.html.twig', [
            'controller_name' => 'UserController',
        ]);
    }

    
    /**
     * @Route("/inscription", name="inscription")
     */
    public function inscription(Request $request, UserRepository $repo, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $form->getData();
            $user->password = $encoder->encodePassword($user, $user->password);
            $repo->add($user);
            dump($user);
            return $this->redirectToRoute("homepage");
        }

        return $this->render('inscription.html.twig', [
            "form" => $form->createView()
        ]);

    }




}

