<?php

namespace App\Controller;

use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;

class SingleArticleController extends Controller
{
    /**
     * @Route("/article/{id}", name="article")
     * 
     */
    public function index(int $id, ArticleRepository $repo, Request $request)
    {
        $article = $repo->get($id);
        $article->image =  new File('uploads/'.$article->image, false);
        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $repo->update($form->getData());
            return $this->redirectToRoute("homepage");
        }
        return $this->render('single_article.html.twig', [
            "form" => $form->createView(),
            "article" => $article
        ]);
    }

    /**
     * @Route("/article/remove/{id}", name = "remove_article")
     */
    public function remove(int $id, ArticleRepository $repo)
    {
        $repo->delete($id);
        return $this->redirectToRoute("homepage");
    }

    /**
     * @Route("/read_article/{id}", name="read_article")
     * 
     */
    public function read(int $id, ArticleRepository $repo, Request $request)
    {
        $article = $repo->get($id);
        $article->image =  new File('uploads/'.$article->image, false);

        return $this->render('read_article.html.twig', [
            "article" => $article
        ]);
    }
}
