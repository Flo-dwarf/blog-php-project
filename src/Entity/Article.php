<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Article
{

  public $title;
  public $category;
  public $image;
  public $content;
  public $id;

  public function __construct($title = null, $category = null, $image = null, $content = null, $id = null)
  {
    $this->title = $title;
    $this->category = $category;
    $this->image = $image;
    $this->content = $content;
    $this->id = $id;
  }

  public static function getAll(array $toCreate) : array
  {
    $output = [];
    foreach ($toCreate as $articleToCreate) {

      $user = new Article($articleToCreate["title"], $articleToCreate["category"], $articleToCreate["image"], $articleToCreate["content"]);

      array_push($output, $article);
    }
    return $output;
  }

  public static function fromSQL(array $rawData)
  {
    return new Article(

      $rawData["title"],
      $rawData["category"],
      $rawData["image"],
      $rawData["content"],
      $rawData["id"]
    );
  }
}