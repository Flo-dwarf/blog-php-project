<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\DependencyInjection\Tests\Compiler\ThrowingExtension;

class User implements UserInterface
{
    public $id;
    public $name;
    public $email;
    public $birthdate;
    /**
     * @Assert\Length(min=5)
     */
    public $password;


    public function __construct(string $name = null, string $email = null, \DateTime $birthdate = null, string $password = null, int $id = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->birthdate = $birthdate;
        $this->password = $password;
    }
    //Une méthode statique est une méthode qu'on peut utiliser directement sur une classe sans avoir besoin de créer d'instance. A la différence d'une méthode classique, on appelle une méthode statique avec avec `::` ex: User::fromSQL()
    public static function fromSQL(array $rawData)
    {
        return new User(
            $rawData["id"],
            $rawData["name"],
            $rawData["email"],
            new \DateTime($rawData["birthdate"]),
            $rawData["password"]
        );
    }
    public function getUsername()
    {
        return $this->email;
    }
    public function getRoles()
    {
        return ["ROLE_USER"];
    }
    public function getPassword()
    {
        return $this->password;
    }
    public function getSalt()
    {

    }
    public function eraseCredentials()
    {
        return null;
    }
}
