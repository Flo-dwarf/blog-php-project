CREATE DATABASE  IF NOT EXISTS `db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db`;
-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `category` varchar(45) NOT NULL,
  `image` blob NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (29,'PoÃ¨me Origami','PoÃ¨me','/tmp/phpiaCmcB','Il a pris dans ses mains le papier,\r\nCarrÃ© nu Ã©purÃ©, fiction platonicienne,\r\nEt comme dÃ©coupÃ© dans un tapis de neige,\r\nPlus vide et plat quâ€™un ocÃ©an de rien.\r\n\r\nLe premier pli parait futile et vain,\r\nDâ€™autant quâ€™il est aussitot mis Ã  plat !\r\nEt les suivants sont tout aussi abscons :\r\nDance des doigts fabricant un chiffon.\r\n\r\nEt puis soudain câ€™est comme une naissance.\r\nDes creux se forment et des coins se dessinent.\r\nOn doute encore de ce que Ã§a peut Ãªtre,\r\nMais quelque chose apparait sous nos yeux.\r\n\r\nLes marques du dÃ©but se font volumes.\r\nDes bouts dÃ©passent encore dâ€™un peu partout.\r\nMais Ã  coups sur par des fentes astucieuses,\r\nIl finiront cachÃ©s. Comment ? MystÃ¨re.\r\n\r\nPeu Ã  peu on comprend ce qui se passe.\r\nUne boite, un bateau ou un chevalâ€¦\r\nLe feuille vide et nue semblait stÃ©rile.\r\nPourtant un monde est nÃ©, sorti dâ€™un rÃªve.\r\n\r\nEt notre monde Ã  nous, quâ€™est-il en fait ?\r\nNous nous croyons volumes et Ã©paisseur.\r\nDe lâ€™atome invisible aux galaxies,\r\nTout sâ€™emboite et vit en trois dimensions.\r\n\r\nImaginez un dÃ©but lisse et plat !\r\nEt que tout ce qui est soit nÃ© de plis\r\nCrÃ©Ã©s par les forces brutes du nÃ©ant,\r\nCourbant la surface infinie du vide.\r\n\r\nLes replis tordent et retordent sans cesse\r\nLa trame impalpable de lâ€™univers.\r\nTant et si bien que le rÃ©el Ã©mergeâ€¦\r\nJusquâ€™Ã  plier la matiÃ¨re et la vie.\r\n\r\nHabitons-nous un grand origami,\r\nAu sein duquel un gÃ©nie nous a mis ?\r\nJe ne sais pas si Ã§a sert dâ€™y penser,\r\nMais jâ€™aime y croire quand je plie du papier.'),(33,'Non?','blabla','/tmp/phpohLlbC','Il faut savoir  que la magie homÃ©opathique  est causÃ©e par un dysfonctionnement quantique des foyers Ã©nergÃ©tiques corporels.\r\n\r\nIl faut faire savoir  que l\'astrologie  est un des Ã©tats mÃ©tapsychiques relevant d\'un surrÃ©el issu des forces cosmiques en prÃ©sence.\r\n\r\nAinsi il est un fait Ã  prendre en compte  que parfois 2 et 2 peuvent ne pas faire 4.'),(34,'Oui?','blabla','/tmp/phpGcFNJH','Sachez  que la schizophrÃ©nie  est une des consÃ©quences d\'une dysharmonie dans la fractalitÃ© des Ã©nergies au milieu du rÃ©seau corporel.\r\n\r\nAjoutons qu\'il est bien entendu dans tous les esprits  que le surnaturel  est une des voies d\'expression de la rÃ©alitÃ© Ã©sotÃ©rique d\'une autre dimension inconnue.\r\n\r\nEn conclusion il n\'est pas interdit de penser et de dire  que l\'objectivitÃ© rÃ©siduelle est ce qui rÃ©siste au balayage de l\'infinie variation des points de vue constituables sur lui.'),(35,'Tigre','Animal','/tmp/phpcJmpOe','Le Tigre (Panthera tigris) est un mammifÃ¨re carnivore de la famille des fÃ©lidÃ©s (Felidae) du genre Panthera. \r\n            AisÃ©ment reconnaissable Ã  sa fourrure rousse rayÃ©e de noir, il est le plus grand fÃ©lin sauvage et l\'un des plus grands carnivores du monde. L\'espÃ¨ce est divisÃ©e en neuf sous-espÃ¨ces possÃ©dant des diffÃ©rences mineures en termes de taille ou de comportement. SuperprÃ©dateur, il chasse principalement les cerfs et les sangliers, bien qu\'il puisse s\'attaquer Ã  des proies de taille plus importante comme les buffles. Jusqu\'au XIXe siÃ¨cle, le Tigre Ã©tait rÃ©putÃ© mangeur d\'homme. La structure sociale des tigres en fait un animal solitaire ; le mÃ¢le possÃ¨de un territoire qui englobe les domaines de plusieurs femelles et ne participe pas Ã  l\'Ã©ducation des petits.');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `birthdate` datetime DEFAULT NULL,
  `password` varchar(65) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (3,'Bloup','toto@bloup.kiki','1997-04-02 00:00:00','$2y$12$YJE0a79DNJqyHdGHHkm2FeRUMoYWzpJ2vmBoaFwsEW.SDejplgeSW');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-26 14:59:32
